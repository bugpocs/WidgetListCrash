#include "widget.h"

Widget::Widget(QWidget *parent)  : QWidget(parent)
{
    this->setMinimumSize(500, 700);

    QListWidgetItem *firstItem = new QListWidgetItem(QIcon::fromTheme("dialog-information"),
                                                     "ONE");
    QListWidgetItem *fourthItem = new QListWidgetItem(QIcon::fromTheme("dialog-error"),
                                                      "FOUR");

    QListWidget *listWidget = new QListWidget(this);
    listWidget->setDragDropMode(QListView::DragDrop);
    listWidget->setDefaultDropAction(Qt::CopyAction);
    listWidget->addItem(firstItem);
    listWidget->addItem("two");
    listWidget->addItem("three");
    listWidget->addItem(fourthItem);

    QLabel *explanationLabel = new QLabel("Try dragging items around.\n\n"
                                          "The program will probably crash if\n"
                                          "you drag the items with icons, to\n"
                                          "their same list, or to the other "
                                          "list.\n\n"
                                          "This doesn't seem to happen when\n"
                                          "building with Qt 4, only with Qt 5.\n\n"
                                          "Items ONE and FOUR should have icons.",
                                          this);
    explanationLabel->setWordWrap(true);


    QListWidget *targetWidget = new QListWidget(this);
    targetWidget->setDragDropMode(QListView::DragDrop);
    targetWidget->setDefaultDropAction(Qt::MoveAction);


    QHBoxLayout *mainLayout = new QHBoxLayout();
    mainLayout->addWidget(listWidget, 1);
    mainLayout->addSpacing(8);
    mainLayout->addWidget(explanationLabel);
    mainLayout->addSpacing(8);
    mainLayout->addWidget(targetWidget, 1);
    this->setLayout(mainLayout);
}

Widget::~Widget()
{

}
